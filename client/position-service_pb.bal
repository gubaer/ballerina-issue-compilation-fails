import ballerina/grpc;

public isolated client class PositionServiceClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function NotifyPosition() returns (NotifyPositionStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("PositionService/NotifyPosition");
        return new NotifyPositionStreamingClient(sClient);
    }
}

public client class NotifyPositionStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendPosition(Position message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextPosition(ContextPosition message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receive() returns grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
        }
    }

    isolated remote function receiveContextNil() returns ContextNil|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class PositionServiceNilCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextPositionStream record {|
    stream<Position, error?> content;
    map<string|string[]> headers;
|};

public type ContextNil record {|
    map<string|string[]> headers;
|};

public type ContextPosition record {|
    Position content;
    map<string|string[]> headers;
|};

public type Empty record {|
|};

public type Position record {|
    float lat = 0.0;
    float lon = 0.0;
|};

const string ROOT_DESCRIPTOR = "0A16706F736974696F6E2D736572766963652E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F222E0A08506F736974696F6E12100A036C617418012001280252036C617412100A036C6F6E18022001280252036C6F6E32480A0F506F736974696F6E5365727669636512350A0E4E6F74696679506F736974696F6E12092E506F736974696F6E1A162E676F6F676C652E70726F746F6275662E456D7074792801620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/empty.proto": "0A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F120F676F6F676C652E70726F746F62756622070A05456D70747942540A13636F6D2E676F6F676C652E70726F746F627566420A456D70747950726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "position-service.proto": "0A16706F736974696F6E2D736572766963652E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F222E0A08506F736974696F6E12100A036C617418012001280252036C617412100A036C6F6E18022001280252036C6F6E32480A0F506F736974696F6E5365727669636512350A0E4E6F74696679506F736974696F6E12092E506F736974696F6E1A162E676F6F676C652E70726F746F6275662E456D7074792801620670726F746F33"};
}

