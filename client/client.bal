import ballerina/log;
import ballerina/lang.runtime;
import ballerina/grpc;

configurable string SERVER_URL = "http://localhost:10001";


public function main() returns error? {
    PositionServiceClient 'client = check new (SERVER_URL);
    NotifyPositionStreamingClient streamingClient = check 'client->NotifyPosition();

    foreach int i in 0...10 {
        log:printInfo(string`sending position ${i} ...`);
        Position pos = {
            lat: <float>i,
            lon: <float>i
        };
        error? e = streamingClient->sendPosition(pos);
        match e {
            var cancelledError if e is grpc:CancelledError => {
                log:printError("stream was cancelled by the server");
                return e;
            }
            // ISSUE: results in a compiler error
            //   error: jVM generation is not supported for type other
            //
            var grpcError if e is grpc:Error => {
                log:printError(string`sendPosition failed. error detected: type=${(typeof e).toString()}`, grpcError);
                return grpcError;
            }
        }
        log:printInfo(string`sleeping for 1 second ...`);
        runtime:sleep(1);
    }
    check streamingClient->complete();
}

