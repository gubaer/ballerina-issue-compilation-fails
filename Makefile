# generates the ballerina records, services, and clients for the
# messages and services described in the .proto file
grpc-generate:
#	not needed in here
# 	bal grpc \
# 		--input position-service.proto \
# 		--output server
# 		--mode server

	bal grpc \
		--input position-service.proto \
		--output client \
		--mode client
# we don't need the sample client
	-rm client/PositionService_sample_client.bal